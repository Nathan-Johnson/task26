﻿using System;

namespace task26
{

    public class Student
    {
        private string _name;
        public string Name
        {
            get {return _name;}
            set {_name = value;}
        }
        private int _id;
        public int Id
        {
            get {return _id;}
            set {_id = value;}
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var s1 = new Student();
            s1.Name = "Joe";
            s1.Id = 12345678;
            Console.WriteLine($"{s1.Name} {s1.Id}");
        }
    }
}
